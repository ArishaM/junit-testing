
//Библиотеки
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

class Okno extends JFrame { // Наследуем JFrame

    // Подготавливаем переменные
    private final JPanel panel;
    private final JButton button;
    private final JTextArea area;
    private final JTextField textArea;

    public Okno() { // Задаем настройки окна
        setSize(400, 1000); // Размер
        setResizable(false); // Изменяемость
        setTitle("Простые числа"); // Имя

        button = new JButton(); // Реализация кнопки
        button.setText("Посчитать"); // Текст на кнопке
        button.addActionListener(this::buttonPressed); // Слушатель
        button.setBounds(10, 500, 230, 60); // Положение и размеры

        area = new JTextArea(5, 10); // Арена для текста
        area.setText("Тут будут циферки");
        area.setBounds(250, 10, 120, 940);
        // Это для красивого переноса
        area.setLineWrap(true);
        area.setWrapStyleWord(true);
        area.setEditable(false); // Не изменяема

        textArea = new JTextField(10); // Поле куда вводдим цифры для их обработки
        textArea.setText("");
        textArea.setBounds(10, 400, 230, 60);

        panel = new JPanel(); // Реализуем panel
        panel.setLayout(null); // Задаем слой
        // Добавление элементов на panel
        panel.add(button);
        panel.add(textArea);
        panel.add(area);

        JScrollPane scrollPane = new JScrollPane(area); // Привязка ползунка
        scrollPane.setViewportView(area); // Устанавливаем привязку к JTextArea
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); // Настройка ползунка
        scrollPane.setBounds(250, 10, 120, 940); // Где должен быть

        panel.add(scrollPane, BorderLayout.CENTER); // добавляем ее на panel

        setContentPane(panel); // Установка panel в Okno
        setDefaultCloseOperation(EXIT_ON_CLOSE); // Выход при закрытии
        setVisible(true); // Видимость
    }

    // Тут просто идет считывание из textArea, вывод в area только простых чисел
    private void buttonPressed(ActionEvent e) {
        StringBuilder sb = new StringBuilder();
        try {
            int k = Integer.parseInt(textArea.getText());

            int currentMax = 1;
            ArrayList<Integer> primeNumbers = getPrimeNumbers(currentMax);

            while (primeNumbers.size() < k) {
                primeNumbers = getPrimeNumbers(currentMax);
                currentMax++;
            }

            for (int i = 0; i < primeNumbers.size(); i++) {
                sb.append(primeNumbers.get(i)).append("\n");
            }

            area.setText(String.valueOf(sb));
        } catch (NumberFormatException ex) { // Если некорректный ввод то выдача ошибки
            JOptionPane.showMessageDialog(Okno.this, "Некорректный ввод данных", "Ошибка",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public static ArrayList<Integer> getPrimeNumbers(int max) {
        ArrayList<Integer> primeNumbers = new ArrayList<Integer>();

        int i = 0;
        int num = 0;

        for (i = 1; i <= max; i++) {
            int counter = 0;
            for (num = i; num >= 1; num--) {
                if (i % num == 0) {
                    counter = counter + 1;
                }
            }
            if (counter == 2) {
                primeNumbers.add(i);
            }
        }

        return primeNumbers;
    }

}