import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class OknoTest {

    ArrayList<Integer> simpleNumbers = new ArrayList<Integer>();

    @Test
    public void getPrimeNumbers() {
        int max = 10;

        simpleNumbers.add(2);
        simpleNumbers.add(3);
        simpleNumbers.add(5);
        simpleNumbers.add(7);

        assertEquals("Ошибка в подсчете массива", simpleNumbers,(Okno.getPrimeNumbers(max)));
    }
}